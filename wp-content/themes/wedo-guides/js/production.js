var $ = jQuery.noConflict()

$(document).ready(function () {
  var footerHeight = $('footer').outerHeight()
  $('.footer-fix').css('padding-bottom', footerHeight)

  $('.section-trigger').click(function (e) {
    e.preventDefault()
    if ($(this).hasClass('active')) {
      $(this).removeClass('active')
      $(this).next('.section-content').slideUp()
    } else {
      $(this).addClass('active')
      $(this).next('.section-content').slideDown()
    }
  })
  $('p:has(img) img').unwrap()
})

var ieVersion = null
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10"
    $('html').addClass('ie-10')
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11"
    $('html').removeClass('ie-10').addClass('ie-11')
}
