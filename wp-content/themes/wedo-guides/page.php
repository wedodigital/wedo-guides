<?php get_header(); ?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>
			<section class="header-grad">
				<svg class="landing-flood">
					<defs>
						<linearGradient id="greenGradient">
	            <stop offset="0" stop-color="#a8e063">
	            	<animate attributeName="stop-color" values="#a8e063;#f83600;#49a09d;#6a3093;#f46b45;#34e89e;#f4c4f3;#ee0979;#a8e063;" dur="30s" repeatCount="indefinite" />
	            </stop>
	            <stop offset="100%" stop-color="#56ab2f">
	            	<animate attributeName="stop-color" values="#56ab2f;#fe8c00;#5f2c82;#a044ff;#eea849;#0f3443;#fc67fa;#ff6a00;#56ab2f;" dur="30s" repeatCount="indefinite" />
	            </stop>
  	        </linearGradient>
					</defs>
					<rect width="100%" height="100%" fill= "url(#greenGradient)" />
				</svg>
			</section>
			<section class="wedo-overlay">
				<article class="wedo-article">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<section class="wedo-docs">
            <?php if ( post_password_required() ) { ?>

            <?php the_content(); ?>

            <?php } else { ?>

              <?php if(get_field('sections')) { ?>
                <?php while(the_repeater_field('sections')) { ?>
                  <a href="#" class="section-trigger"><?php the_sub_field('section_title'); ?></a>
                  <div class="section-content">
                    <?php the_sub_field('section_content'); ?>
                  </div>
                <?php } ?>
              <?php } ?>

            <?php } ?>
          </section>
					<div class="footer-w"></div>
				</article>
			</section>
		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
